# No Audio

## Install

```
pip install git+ssh://git@gitlab.com/jabaliesrc/videotools/noaudio.git@main
```

## Use

```
na --path /mnt/jrc/crudo/JRC-SANMA-11-09.mp4
```

will output a copy of `/mnt/jrc/crudo/JRC-SANMA-11-09.mp4` with the audio removed to `/mnt/jrc/crudo/JRC-SANMA-11-09.na.mp4`

#### Optional

Can be run with the `--output` flag to specify the desired output video file