import os
from distutils.core import setup
from setuptools import find_packages

required = [
    'click',
    'ffmpeg-python',
]

setup(
    name='noaudio',
    version='0.1',
    author=u'Teofilo Sibileau',
    author_email='teo.sibileau@gmail.com',
    license='MIT license, see LICENSE',
    description='Removes audio from video files',
    packages=['src'],
    include_package_data=True,
    zip_safe=False,
    entry_points = {
        'console_scripts': [
            'na = src.na:na',
        ],
    },
    install_requires=required,
)