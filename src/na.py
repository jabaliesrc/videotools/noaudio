import ffmpeg
import click
import os


@click.command()
@click.option('--video', required=True, help='Input video')
@click.option('--output', required=False, help='Output video')
def na(video, output):
    if not output:
        output = video.split('.')
        output[-1] = 'na.mp4'
        output = '.'.join(output)

    out, err = (
         ffmpeg.input(video, an=None)
               .output(output, c='copy')
               .run()
    )

if __name__ == '__main__':
    na()